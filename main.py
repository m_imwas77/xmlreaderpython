readFile = open("dataInput.xml","r")
writeFile = open("dataOutPut.xlsx","w")
l = []
ans = []
sol = ""
sourse = ""
def read():
    global sol
    for line in readFile:
        line = line.strip()
        line_split = line.split()
        for data in line_split:
            if(data == "source=\"Name\""):
                sourse = "Name"
                break
            if(data == "source=\"Definition\""):
                sourse = "Definition"
                break
        if(len(line) > 0 and line[0] != '<'):
            sol = sol + line + '\n'
            continue
        if(len(line) == 0):
            sol  = sol + '\n'
            continue
        if(line == "</xs:documentation>"):
            l.append("</xs:documentation>")
            ans.append((sourse,sol))
            sol = ""
            l.pop()
        elif(line[0] == '<'):
            sub1 = "<xs:documentation"
            sub2 = sub1 + '>'
            sol2 = ""
            if(len(line) > len(sub1)):
                if line[0:len(sub1)] == sub1 :
                    l.append("<xs:documentation>")
                    for i in range(len(line)):
                        if(line[i] == '>'):
                            for j in range(i+1,len(line)):
                                if(line[j] == '<'):
                                    break
                                sol2 = sol2 + line[j]
                        if(line[i] == '<' and line[i+1] == '/'):
                            l.append("</xs:documentation>")
                            ans.append((sourse,sol2))
                            l.pop()
                            break
def write():
    for data in ans:
        writeFile.write(data[0]+data[1]+'\n')
def main():
    read()
    write()
if __name__ == '__main__':
    main()
